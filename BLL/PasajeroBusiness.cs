﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class PasajeroBusiness
    {
        McPasajero mc = new McPasajero();
        public void Alta(PasajeroEntity pasajero)
        {
            mc.Alta(pasajero);
        }

        public void Baja(PasajeroEntity pasajero)
        {
            mc.Baja(pasajero);
        }

        public void Modificacion(PasajeroEntity pasajero)
        {
            mc.Modifiacion(pasajero);
        }

        public List<PasajeroEntity> Listar()
        {
            return mc.Listar();
        }
    }
}

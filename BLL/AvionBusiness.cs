﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;
namespace BLL
{
    public class AvionBusiness
    {
         McAvion mc = new McAvion();
        public AvionEntity ObtenerAvion(int id)
        {
            return mc.ObtenerAvion(id);
        }

        public List<AvionEntity> Listar()
        {
            return mc.Listar();
        }

        public void Alta(AvionEntity avion)
        {
            mc.Alta(avion);
        }

        public void Baja(AvionEntity avion)
        {
            mc.Baja(avion);
        }

        public void Modificacion(AvionEntity avion)
        {
            mc.Modifiacion(avion);
        }
    }
}

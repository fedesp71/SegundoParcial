﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class CiudadBusiness
    {
        McCiudad mc = new McCiudad();
        public List<CiudadEntity> Listar()
        {
            return mc.Listar();
        }

        public void Alta(CiudadEntity avion)
        {
            mc.Alta(avion);
        }

        public void Baja(CiudadEntity avion)
        {
            mc.Baja(avion);
        }

        public void Modificacion(CiudadEntity avion)
        {
            mc.Modifiacion(avion);
        }
    }
}

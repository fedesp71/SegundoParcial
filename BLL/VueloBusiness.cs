﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class VueloBusiness
    {
        McVuelo mc = new McVuelo();
        public void Alta(VueloEntity vuelo)
        {
            mc.Alta(vuelo);
        }

        public void AltaPasajeros(VueloEntity vuelo, List<PasajeroEntity> pasajeros)
        {
            foreach(PasajeroEntity pasajero in pasajeros)
            {
                mc.AltaPasajero(vuelo, pasajero);
            }
        }

        public void Modificacion(VueloEntity vuelo)
        {
           // mc.Modificacion(vuelo);
        }

        public void CancelarVuelo(VueloEntity vuelo)
        {
            mc.Cancelacion(vuelo);
        }

        public List<VueloEntity> Listar()
        {
            return mc.Listar();
        }
        public List<VueloEntity> Listar40()
        {
            return mc.Listar40();
        }


        //public List<VueloEntity> ListarVuelosxCombinacion(VueloEntity vuelo)
        //{
        //    //return mc.ListarVuelosxCombinacion(vuelo);
        //}
    }
}

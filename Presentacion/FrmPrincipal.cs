﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void pasajeroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPasajero frmPasajero = new FrmPasajero();
            frmPasajero.MdiParent = this;
            frmPasajero.Show();
        }

        private void avionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAvion frmAvion = new FrmAvion();
            frmAvion.MdiParent = this;
            frmAvion.Show();
        }

        private void vueloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVuelo frmVuelo = new FrmVuelo();
            frmVuelo.MdiParent = this;
            frmVuelo.Show();
        }

        private void ciudadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCiudad frmCiudad = new FrmCiudad();
            frmCiudad.MdiParent = this;
            frmCiudad.Show();
        }

        private void mapaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmMapa frmMapa = new FrmMapa();
            frmMapa.MdiParent = this;
            frmMapa.Show();
        }

        private void abordarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmAbordar frmAbordar = new FrmAbordar();
            frmAbordar.MdiParent = this;
            frmAbordar.Show();
        }

        private void vuelos40ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmVuelo40 frmVuelo40 = new FrmVuelo40();
            frmVuelo40.MdiParent = this;
            frmVuelo40.Show();
        }
    }
}

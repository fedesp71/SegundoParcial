﻿namespace Presentacion
{
    partial class FrmVuelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtAvion = new System.Windows.Forms.DataGridView();
            this.dtVuelo = new System.Windows.Forms.DataGridView();
            this.ucABMvuelo = new Presentacion.ucABM();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.dtDesde = new System.Windows.Forms.DateTimePicker();
            this.dtHasta = new System.Windows.Forms.DateTimePicker();
            this.dtCiudad = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dtAvion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCiudad)).BeginInit();
            this.SuspendLayout();
            // 
            // dtAvion
            // 
            this.dtAvion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtAvion.Location = new System.Drawing.Point(145, 33);
            this.dtAvion.Name = "dtAvion";
            this.dtAvion.Size = new System.Drawing.Size(240, 150);
            this.dtAvion.TabIndex = 1;
            // 
            // dtVuelo
            // 
            this.dtVuelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtVuelo.Location = new System.Drawing.Point(299, 207);
            this.dtVuelo.Name = "dtVuelo";
            this.dtVuelo.Size = new System.Drawing.Size(240, 150);
            this.dtVuelo.TabIndex = 3;
            // 
            // ucABMvuelo
            // 
            this.ucABMvuelo.Location = new System.Drawing.Point(12, 12);
            this.ucABMvuelo.Name = "ucABMvuelo";
            this.ucABMvuelo.Size = new System.Drawing.Size(107, 131);
            this.ucABMvuelo.TabIndex = 0;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(299, 366);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutar.TabIndex = 4;
            this.btnEjecutar.Text = "Ejecutar";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // dtDesde
            // 
            this.dtDesde.Location = new System.Drawing.Point(25, 207);
            this.dtDesde.Name = "dtDesde";
            this.dtDesde.Size = new System.Drawing.Size(94, 20);
            this.dtDesde.TabIndex = 5;
            // 
            // dtHasta
            // 
            this.dtHasta.Location = new System.Drawing.Point(25, 259);
            this.dtHasta.Name = "dtHasta";
            this.dtHasta.Size = new System.Drawing.Size(94, 20);
            this.dtHasta.TabIndex = 6;
            // 
            // dtCiudad
            // 
            this.dtCiudad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtCiudad.Location = new System.Drawing.Point(440, 33);
            this.dtCiudad.Name = "dtCiudad";
            this.dtCiudad.Size = new System.Drawing.Size(240, 150);
            this.dtCiudad.TabIndex = 7;
            // 
            // FrmVuelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 410);
            this.Controls.Add(this.dtCiudad);
            this.Controls.Add(this.dtHasta);
            this.Controls.Add(this.dtDesde);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.dtVuelo);
            this.Controls.Add(this.dtAvion);
            this.Controls.Add(this.ucABMvuelo);
            this.Name = "FrmVuelo";
            this.Text = "FrmVuelo";
            this.Load += new System.EventHandler(this.FrmVuelo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtAvion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCiudad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ucABM ucABMvuelo;
        private System.Windows.Forms.DataGridView dtAvion;
        private System.Windows.Forms.DataGridView dtVuelo;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.DateTimePicker dtDesde;
        private System.Windows.Forms.DateTimePicker dtHasta;
        private System.Windows.Forms.DataGridView dtCiudad;
    }
}
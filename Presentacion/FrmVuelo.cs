﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmVuelo : Form
    {
        public FrmVuelo()
        {
            InitializeComponent();
        }

        AvionBusiness gestorAvion = new AvionBusiness();
        PasajeroBusiness gestorPasajero = new PasajeroBusiness();
        VueloBusiness gestorVuelo = new VueloBusiness();
        CiudadBusiness gestorCiudad = new CiudadBusiness();
        private void FrmVuelo_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        public void LlenarGrilla()
        {
            dtAvion.DataSource = null;
            dtAvion.DataSource = gestorAvion.Listar();


            //dtPasajero.DataSource = null;
            //dtPasajero.DataSource = gestorPasajero.Listar();


            dtVuelo.DataSource = null;
            dtVuelo.DataSource = gestorVuelo.Listar();

            dtCiudad.DataSource = null;
            dtCiudad.DataSource = gestorCiudad.Listar();

        }


        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            VueloEntity vuelo = new VueloEntity();
            //pasajero.Nombre = txtNombrePasajero.Text;
            vuelo.HoraPartida = dtHasta.Value;
            vuelo.HoraArribo = dtDesde.Value;
            if (ucABMvuelo.OpcionElegida() == 0)
            {
                AvionEntity avion = (AvionEntity)dtAvion.SelectedRows[0].DataBoundItem;
                CiudadEntity ciudad = (CiudadEntity)dtCiudad.SelectedRows[0].DataBoundItem;
                vuelo.Destino = ciudad;
                vuelo.Avion = avion;
                //vuelo.Pasajeros = pasajeros();
                gestorVuelo.Alta(vuelo);
                //gestorVuelo.AltaPasajeros(vuelo, vuelo.Pasajeros);
                LlenarGrilla();
            }
            if (ucABMvuelo.OpcionElegida() == 1)
            {

                VueloEntity pasajeroEliminar = (VueloEntity)dtVuelo.SelectedRows[0].DataBoundItem;
                gestorVuelo.CancelarVuelo(pasajeroEliminar);
                LlenarGrilla();

            }
            if (ucABMvuelo.OpcionElegida() == 2)
            {
                
                gestorVuelo.Modificacion(vuelo);
                LlenarGrilla();
            }
        }
    }
}

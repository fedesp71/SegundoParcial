﻿namespace Presentacion
{
    partial class FrmAvion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucABMavion = new Presentacion.ucABM();
            this.dtAvion = new System.Windows.Forms.DataGridView();
            this.txtNombreAvion = new System.Windows.Forms.TextBox();
            this.ndAvion = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnEjecutar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtAvion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ndAvion)).BeginInit();
            this.SuspendLayout();
            // 
            // ucABMavion
            // 
            this.ucABMavion.Location = new System.Drawing.Point(31, 12);
            this.ucABMavion.Name = "ucABMavion";
            this.ucABMavion.Size = new System.Drawing.Size(107, 131);
            this.ucABMavion.TabIndex = 0;
            // 
            // dtAvion
            // 
            this.dtAvion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtAvion.Location = new System.Drawing.Point(375, 12);
            this.dtAvion.Name = "dtAvion";
            this.dtAvion.Size = new System.Drawing.Size(240, 150);
            this.dtAvion.TabIndex = 1;
            // 
            // txtNombreAvion
            // 
            this.txtNombreAvion.Location = new System.Drawing.Point(209, 46);
            this.txtNombreAvion.Name = "txtNombreAvion";
            this.txtNombreAvion.Size = new System.Drawing.Size(136, 20);
            this.txtNombreAvion.TabIndex = 2;
            // 
            // ndAvion
            // 
            this.ndAvion.Location = new System.Drawing.Point(209, 90);
            this.ndAvion.Name = "ndAvion";
            this.ndAvion.Size = new System.Drawing.Size(136, 20);
            this.ndAvion.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Capacidad";
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(375, 187);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutar.TabIndex = 6;
            this.btnEjecutar.Text = "Ejecutar";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // FrmAvion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 243);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ndAvion);
            this.Controls.Add(this.txtNombreAvion);
            this.Controls.Add(this.dtAvion);
            this.Controls.Add(this.ucABMavion);
            this.Name = "FrmAvion";
            this.Text = "FrmAvion";
            this.Load += new System.EventHandler(this.FrmAvion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtAvion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ndAvion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ucABM ucABMavion;
        private System.Windows.Forms.DataGridView dtAvion;
        private System.Windows.Forms.TextBox txtNombreAvion;
        private System.Windows.Forms.NumericUpDown ndAvion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnEjecutar;
    }
}
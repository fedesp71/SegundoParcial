﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmVuelosLibres : Form
    {
        public FrmVuelosLibres()
        {
            InitializeComponent();
        }
        CiudadBusiness gestorCiudad = new CiudadBusiness();
        VueloBusiness gestorVuelo = new VueloBusiness();
        private void FrmVuelosLibres_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        public void LlenarGrilla()
        {

            dtCiudad.DataSource = null;
            dtCiudad.DataSource = gestorCiudad.Listar();


            dtVuelo.DataSource = null;
            dtVuelo.DataSource = gestorVuelo.Listar();
        }
    }
}

﻿namespace Presentacion
{
    partial class FrmMapa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtCiudad = new System.Windows.Forms.DataGridView();
            this.btnMapa = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtCiudad)).BeginInit();
            this.SuspendLayout();
            // 
            // dtCiudad
            // 
            this.dtCiudad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtCiudad.Location = new System.Drawing.Point(27, 12);
            this.dtCiudad.Name = "dtCiudad";
            this.dtCiudad.Size = new System.Drawing.Size(486, 150);
            this.dtCiudad.TabIndex = 0;
            // 
            // btnMapa
            // 
            this.btnMapa.Location = new System.Drawing.Point(83, 213);
            this.btnMapa.Name = "btnMapa";
            this.btnMapa.Size = new System.Drawing.Size(75, 23);
            this.btnMapa.TabIndex = 1;
            this.btnMapa.Text = "Mapa";
            this.btnMapa.UseVisualStyleBackColor = true;
            this.btnMapa.Click += new System.EventHandler(this.btnMapa_Click);
            // 
            // FrmMapa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 450);
            this.Controls.Add(this.btnMapa);
            this.Controls.Add(this.dtCiudad);
            this.Name = "FrmMapa";
            this.Text = "FrmMapa";
            this.Load += new System.EventHandler(this.FrmMapa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtCiudad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtCiudad;
        private System.Windows.Forms.Button btnMapa;
    }
}
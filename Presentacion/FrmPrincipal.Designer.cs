﻿namespace Presentacion
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aBMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasajeroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vueloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ciudadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abordarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abordarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vuelos40ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vuelos40ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMToolStripMenuItem,
            this.mapaToolStripMenuItem,
            this.abordarToolStripMenuItem,
            this.vuelos40ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aBMToolStripMenuItem
            // 
            this.aBMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pasajeroToolStripMenuItem,
            this.avionToolStripMenuItem,
            this.vueloToolStripMenuItem,
            this.ciudadToolStripMenuItem});
            this.aBMToolStripMenuItem.Name = "aBMToolStripMenuItem";
            this.aBMToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.aBMToolStripMenuItem.Text = "ABM";
            // 
            // pasajeroToolStripMenuItem
            // 
            this.pasajeroToolStripMenuItem.Name = "pasajeroToolStripMenuItem";
            this.pasajeroToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.pasajeroToolStripMenuItem.Text = "Pasajero";
            this.pasajeroToolStripMenuItem.Click += new System.EventHandler(this.pasajeroToolStripMenuItem_Click);
            // 
            // avionToolStripMenuItem
            // 
            this.avionToolStripMenuItem.Name = "avionToolStripMenuItem";
            this.avionToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.avionToolStripMenuItem.Text = "Avion";
            this.avionToolStripMenuItem.Click += new System.EventHandler(this.avionToolStripMenuItem_Click);
            // 
            // vueloToolStripMenuItem
            // 
            this.vueloToolStripMenuItem.Name = "vueloToolStripMenuItem";
            this.vueloToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.vueloToolStripMenuItem.Text = "Vuelo";
            this.vueloToolStripMenuItem.Click += new System.EventHandler(this.vueloToolStripMenuItem_Click);
            // 
            // ciudadToolStripMenuItem
            // 
            this.ciudadToolStripMenuItem.Name = "ciudadToolStripMenuItem";
            this.ciudadToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.ciudadToolStripMenuItem.Text = "Ciudad";
            this.ciudadToolStripMenuItem.Click += new System.EventHandler(this.ciudadToolStripMenuItem_Click);
            // 
            // mapaToolStripMenuItem
            // 
            this.mapaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mapaToolStripMenuItem1});
            this.mapaToolStripMenuItem.Name = "mapaToolStripMenuItem";
            this.mapaToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.mapaToolStripMenuItem.Text = "Mapa";
            // 
            // mapaToolStripMenuItem1
            // 
            this.mapaToolStripMenuItem1.Name = "mapaToolStripMenuItem1";
            this.mapaToolStripMenuItem1.Size = new System.Drawing.Size(104, 22);
            this.mapaToolStripMenuItem1.Text = "Mapa";
            this.mapaToolStripMenuItem1.Click += new System.EventHandler(this.mapaToolStripMenuItem1_Click);
            // 
            // abordarToolStripMenuItem
            // 
            this.abordarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abordarToolStripMenuItem1});
            this.abordarToolStripMenuItem.Name = "abordarToolStripMenuItem";
            this.abordarToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.abordarToolStripMenuItem.Text = "Abordar";
            // 
            // abordarToolStripMenuItem1
            // 
            this.abordarToolStripMenuItem1.Name = "abordarToolStripMenuItem1";
            this.abordarToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.abordarToolStripMenuItem1.Text = "Abordar";
            this.abordarToolStripMenuItem1.Click += new System.EventHandler(this.abordarToolStripMenuItem1_Click);
            // 
            // vuelos40ToolStripMenuItem
            // 
            this.vuelos40ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vuelos40ToolStripMenuItem1});
            this.vuelos40ToolStripMenuItem.Name = "vuelos40ToolStripMenuItem";
            this.vuelos40ToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.vuelos40ToolStripMenuItem.Text = "Vuelos40";
            // 
            // vuelos40ToolStripMenuItem1
            // 
            this.vuelos40ToolStripMenuItem1.Name = "vuelos40ToolStripMenuItem1";
            this.vuelos40ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.vuelos40ToolStripMenuItem1.Text = "Vuelos40";
            this.vuelos40ToolStripMenuItem1.Click += new System.EventHandler(this.vuelos40ToolStripMenuItem1_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipal";
            this.Text = "FrmPrincipal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aBMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasajeroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vueloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ciudadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abordarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abordarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vuelos40ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vuelos40ToolStripMenuItem1;
    }
}
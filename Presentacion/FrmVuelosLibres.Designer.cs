﻿namespace Presentacion
{
    partial class FrmVuelosLibres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtVuelo = new System.Windows.Forms.DataGridView();
            this.dtCiudad = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCiudad)).BeginInit();
            this.SuspendLayout();
            // 
            // dtVuelo
            // 
            this.dtVuelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtVuelo.Location = new System.Drawing.Point(309, 12);
            this.dtVuelo.Name = "dtVuelo";
            this.dtVuelo.Size = new System.Drawing.Size(509, 150);
            this.dtVuelo.TabIndex = 2;
            // 
            // dtCiudad
            // 
            this.dtCiudad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtCiudad.Location = new System.Drawing.Point(12, 12);
            this.dtCiudad.Name = "dtCiudad";
            this.dtCiudad.Size = new System.Drawing.Size(240, 150);
            this.dtCiudad.TabIndex = 3;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(12, 211);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            // 
            // FrmVuelosLibres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 450);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.dtCiudad);
            this.Controls.Add(this.dtVuelo);
            this.Name = "FrmVuelosLibres";
            this.Text = "FrmVuelosLibres";
            this.Load += new System.EventHandler(this.FrmVuelosLibres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCiudad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dtVuelo;
        private System.Windows.Forms.DataGridView dtCiudad;
        private System.Windows.Forms.Button btnBuscar;
    }
}
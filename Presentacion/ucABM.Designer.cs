﻿namespace Presentacion
{
    partial class ucABM
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdAlta = new System.Windows.Forms.RadioButton();
            this.rdBaja = new System.Windows.Forms.RadioButton();
            this.rdModif = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // rdAlta
            // 
            this.rdAlta.AutoSize = true;
            this.rdAlta.Location = new System.Drawing.Point(3, 16);
            this.rdAlta.Name = "rdAlta";
            this.rdAlta.Size = new System.Drawing.Size(43, 17);
            this.rdAlta.TabIndex = 0;
            this.rdAlta.TabStop = true;
            this.rdAlta.Text = "Alta";
            this.rdAlta.UseVisualStyleBackColor = true;
            this.rdAlta.CheckedChanged += new System.EventHandler(this.rdAlta_CheckedChanged);
            // 
            // rdBaja
            // 
            this.rdBaja.AutoSize = true;
            this.rdBaja.Location = new System.Drawing.Point(3, 57);
            this.rdBaja.Name = "rdBaja";
            this.rdBaja.Size = new System.Drawing.Size(46, 17);
            this.rdBaja.TabIndex = 1;
            this.rdBaja.TabStop = true;
            this.rdBaja.Text = "Baja";
            this.rdBaja.UseVisualStyleBackColor = true;
            this.rdBaja.CheckedChanged += new System.EventHandler(this.rdBaja_CheckedChanged);
            // 
            // rdModif
            // 
            this.rdModif.AutoSize = true;
            this.rdModif.Location = new System.Drawing.Point(3, 100);
            this.rdModif.Name = "rdModif";
            this.rdModif.Size = new System.Drawing.Size(68, 17);
            this.rdModif.TabIndex = 2;
            this.rdModif.TabStop = true;
            this.rdModif.Text = "Modificar";
            this.rdModif.UseVisualStyleBackColor = true;
            this.rdModif.CheckedChanged += new System.EventHandler(this.rdModif_CheckedChanged);
            // 
            // ucABM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rdModif);
            this.Controls.Add(this.rdBaja);
            this.Controls.Add(this.rdAlta);
            this.Name = "ucABM";
            this.Size = new System.Drawing.Size(107, 131);
            this.Load += new System.EventHandler(this.ucABM_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdAlta;
        private System.Windows.Forms.RadioButton rdBaja;
        private System.Windows.Forms.RadioButton rdModif;
    }
}

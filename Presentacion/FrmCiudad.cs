﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using BE;

namespace Presentacion
{
    public partial class FrmCiudad : Form
    {
        public FrmCiudad()
        {
            InitializeComponent();
        }

        CiudadBusiness gestor = new CiudadBusiness();
        private void FrmCiudad_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }


        public void LlenarGrilla()
        {
            dtCiudad.DataSource = null;
            dtCiudad.DataSource = gestor.Listar();

        }

        private void btEjecutar_Click(object sender, EventArgs e)
        {
            CiudadEntity ciudad = new CiudadEntity();

            if (ucABMciudad.OpcionElegida() == 0)
            {
                ciudad.Nombre = txtNombre.Text;
                ciudad.Latitud = txtLatitud.Text;
                ciudad.Longitud = txtLongitud.Text;
                gestor.Alta(ciudad);
                LlenarGrilla();
            }
            if (ucABMciudad.OpcionElegida() == 1)
            {

                CiudadEntity pasajeroEliminar = (CiudadEntity)dtCiudad.SelectedRows[0].DataBoundItem;
                gestor.Baja(pasajeroEliminar);
                LlenarGrilla();

            }
            if (ucABMciudad.OpcionElegida() == 2)
            {
                ciudad.Nombre = txtNombre.Text;
                ciudad.Latitud = txtLatitud.Text;
                ciudad.Longitud = txtLongitud.Text;
                gestor.Modificacion(ciudad);
                LlenarGrilla();
            }
        }
    }
}

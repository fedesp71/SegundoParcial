﻿namespace Presentacion
{
    partial class FrmPasajero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombrePasajero = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ucABMpasajero = new Presentacion.ucABM();
            this.dtPasajeros = new System.Windows.Forms.DataGridView();
            this.btnEjecutar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtPasajeros)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNombrePasajero
            // 
            this.txtNombrePasajero.Location = new System.Drawing.Point(198, 34);
            this.txtNombrePasajero.Name = "txtNombrePasajero";
            this.txtNombrePasajero.Size = new System.Drawing.Size(100, 20);
            this.txtNombrePasajero.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre";
            // 
            // ucABMpasajero
            // 
            this.ucABMpasajero.Location = new System.Drawing.Point(12, 34);
            this.ucABMpasajero.Name = "ucABMpasajero";
            this.ucABMpasajero.Size = new System.Drawing.Size(107, 131);
            this.ucABMpasajero.TabIndex = 2;
            // 
            // dtPasajeros
            // 
            this.dtPasajeros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtPasajeros.Location = new System.Drawing.Point(304, 15);
            this.dtPasajeros.Name = "dtPasajeros";
            this.dtPasajeros.Size = new System.Drawing.Size(240, 150);
            this.dtPasajeros.TabIndex = 3;
            this.dtPasajeros.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtPasajeros_CellClick);
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(304, 185);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutar.TabIndex = 4;
            this.btnEjecutar.Text = "Ejecutar";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // FrmPasajero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 235);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.dtPasajeros);
            this.Controls.Add(this.ucABMpasajero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNombrePasajero);
            this.Name = "FrmPasajero";
            this.Text = "FrmPasajero";
            this.Load += new System.EventHandler(this.FrmPasajero_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtPasajeros)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombrePasajero;
        private System.Windows.Forms.Label label1;
        private ucABM ucABMpasajero;
        private System.Windows.Forms.DataGridView dtPasajeros;
        private System.Windows.Forms.Button btnEjecutar;
    }
}
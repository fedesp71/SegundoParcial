﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmAbordar : Form
    {
        public FrmAbordar()
        {
            InitializeComponent();
        }
        PasajeroBusiness gestorPasajero = new PasajeroBusiness();
        VueloBusiness gestorVuelo = new VueloBusiness();
        private void FrmAbordar_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        public List<PasajeroEntity> pasajeros()
        {
            List<PasajeroEntity> pasajeros = new List<PasajeroEntity>();
            foreach (DataGridViewRow dr in dtPasajero.SelectedRows)
            {

                PasajeroEntity pasajero = new PasajeroEntity();
                pasajero.Id = (int)dr.Cells[0].Value;
                pasajero.Nombre = (string)dr.Cells[1].Value;
                pasajeros.Add(pasajero);
            }
            return pasajeros;
        }

        public void LlenarGrilla()
        {


            dtPasajero.DataSource = null;
            dtPasajero.DataSource = gestorPasajero.Listar();


            dtVuelo.DataSource = null;
            dtVuelo.DataSource = gestorVuelo.Listar();


        }

        private void btnAbordar_Click(object sender, EventArgs e)
        {

            VueloEntity vuelo = (VueloEntity)dtVuelo.SelectedRows[0].DataBoundItem;
            gestorVuelo.AltaPasajeros(vuelo, pasajeros());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmVuelo40 : Form
    {
        public FrmVuelo40()
        {
            InitializeComponent();
        }

        private void FrmVuelo40_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        VueloBusiness gestor = new VueloBusiness();
        public void LlenarGrilla()
        {
            dtVuelos40.DataSource = null;
            dtVuelos40.DataSource = gestor.Listar40();
        }
    }
}

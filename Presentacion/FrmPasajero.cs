﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmPasajero : Form
    {
        public FrmPasajero()
        {
            InitializeComponent();
        }

        PasajeroBusiness gestor = new PasajeroBusiness();
        private void FrmPasajero_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        public void LlenarGrilla()
        {
            dtPasajeros.DataSource = null;
            dtPasajeros.DataSource = gestor.Listar();
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            PasajeroEntity pasajero = new PasajeroEntity();
            pasajero.Nombre = txtNombrePasajero.Text;
            if(ucABMpasajero.OpcionElegida() == 0)
            {
                gestor.Alta(pasajero);
                LlenarGrilla();
            }
            if (ucABMpasajero.OpcionElegida() == 1)
            {

                PasajeroEntity pasajeroEliminar = (PasajeroEntity)dtPasajeros.SelectedRows[0].DataBoundItem;
                gestor.Baja(pasajeroEliminar);
                LlenarGrilla();

            }
            if (ucABMpasajero.OpcionElegida() == 2)
            {
                pasajero.Nombre = txtNombrePasajero.Text;
                gestor.Modificacion(pasajero);
                LlenarGrilla();
            }
        }

        private void dtPasajeros_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PasajeroEntity pasajero = (PasajeroEntity)dtPasajeros.SelectedRows[0].DataBoundItem;
            txtNombrePasajero.Text = pasajero.Nombre;
        }
    }
}

﻿namespace Presentacion
{
    partial class FrmAbordar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtPasajero = new System.Windows.Forms.DataGridView();
            this.dtVuelo = new System.Windows.Forms.DataGridView();
            this.btnAbordar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtPasajero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelo)).BeginInit();
            this.SuspendLayout();
            // 
            // dtPasajero
            // 
            this.dtPasajero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtPasajero.Location = new System.Drawing.Point(95, 70);
            this.dtPasajero.Name = "dtPasajero";
            this.dtPasajero.Size = new System.Drawing.Size(240, 150);
            this.dtPasajero.TabIndex = 0;
            // 
            // dtVuelo
            // 
            this.dtVuelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtVuelo.Location = new System.Drawing.Point(451, 70);
            this.dtVuelo.Name = "dtVuelo";
            this.dtVuelo.Size = new System.Drawing.Size(240, 150);
            this.dtVuelo.TabIndex = 1;
            // 
            // btnAbordar
            // 
            this.btnAbordar.Location = new System.Drawing.Point(369, 272);
            this.btnAbordar.Name = "btnAbordar";
            this.btnAbordar.Size = new System.Drawing.Size(75, 23);
            this.btnAbordar.TabIndex = 2;
            this.btnAbordar.Text = "Abordar";
            this.btnAbordar.UseVisualStyleBackColor = true;
            this.btnAbordar.Click += new System.EventHandler(this.btnAbordar_Click);
            // 
            // FrmAbordar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAbordar);
            this.Controls.Add(this.dtVuelo);
            this.Controls.Add(this.dtPasajero);
            this.Name = "FrmAbordar";
            this.Text = "FrmAbordar";
            this.Load += new System.EventHandler(this.FrmAbordar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtPasajero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtPasajero;
        private System.Windows.Forms.DataGridView dtVuelo;
        private System.Windows.Forms.Button btnAbordar;
    }
}
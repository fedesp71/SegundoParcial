﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmAvion : Form
    {
        public FrmAvion()
        {
            InitializeComponent();
        }

        AvionBusiness gestor = new AvionBusiness();
        private void FrmAvion_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        public void LlenarGrilla()
        {
            dtAvion.DataSource = null;
            dtAvion.DataSource = gestor.Listar();
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            AvionEntity avion = new AvionEntity();
            avion.Nombre = txtNombreAvion.Text;
            if (ucABMavion.OpcionElegida() == 0)
            {
                avion.Nombre = txtNombreAvion.Text;
                avion.Capacidad = int.Parse(ndAvion.Value.ToString());
                gestor.Alta(avion);
                LlenarGrilla();
            }
            if (ucABMavion.OpcionElegida() == 1)
            {

                AvionEntity avionEliminar = (AvionEntity)dtAvion.SelectedRows[0].DataBoundItem;
                gestor.Baja(avionEliminar);
                LlenarGrilla();

            }
            if (ucABMavion.OpcionElegida() == 2)
            {
                avion.Nombre = txtNombreAvion.Text;
                gestor.Modificacion(avion);
                LlenarGrilla();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ucABM : UserControl
    {
        public ucABM()
        {
            InitializeComponent();
        }
        public int opcion = 0;

        private void rdAlta_CheckedChanged(object sender, EventArgs e)
        {
            opcion = 0;
        }

        public int OpcionElegida()
        {
            return opcion;
        }

        private void rdBaja_CheckedChanged(object sender, EventArgs e)
        {
            opcion = 1;
        }

        private void rdModif_CheckedChanged(object sender, EventArgs e)
        {
            opcion = 2;
        }

        private void ucABM_Load(object sender, EventArgs e)
        {

        }
    }
}

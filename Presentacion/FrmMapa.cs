﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace Presentacion
{
    public partial class FrmMapa : Form
    {
        public FrmMapa()
        {
            InitializeComponent();
        }

        private void FrmMapa_Load(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        CiudadBusiness gestorCiudad = new CiudadBusiness();
        public void LlenarGrilla()
        {

            dtCiudad.DataSource = null;
            dtCiudad.DataSource = gestorCiudad.Listar();

        }

        private void btnMapa_Click(object sender, EventArgs e)
        {
            CiudadEntity ciudad = (CiudadEntity)dtCiudad.SelectedRows[0].DataBoundItem;
            string url = "https://www.google.com/maps/@" + ciudad.Latitud + "," + ciudad.Longitud + ",13z/";
            
            System.Diagnostics.Process.Start(url);
        }
    }
}
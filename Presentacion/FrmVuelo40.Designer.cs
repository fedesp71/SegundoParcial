﻿namespace Presentacion
{
    partial class FrmVuelo40
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtVuelos40 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelos40)).BeginInit();
            this.SuspendLayout();
            // 
            // dtVuelos40
            // 
            this.dtVuelos40.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtVuelos40.Location = new System.Drawing.Point(12, 36);
            this.dtVuelos40.Name = "dtVuelos40";
            this.dtVuelos40.Size = new System.Drawing.Size(401, 150);
            this.dtVuelos40.TabIndex = 0;
            // 
            // FrmVuelo40
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 217);
            this.Controls.Add(this.dtVuelos40);
            this.Name = "FrmVuelo40";
            this.Text = "FrmVuelo40";
            this.Load += new System.EventHandler(this.FrmVuelo40_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtVuelos40)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtVuelos40;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class PasajeroEntity
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private List<VueloEntity> vuelos;

        public List<VueloEntity> Vuelos
        {
            get { return vuelos; }
            set { vuelos = value; }
        }



    }
}

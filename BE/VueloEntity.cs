﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class VueloEntity
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private CiudadEntity destino;

        public CiudadEntity Destino
        {
            get { return destino; }
            set { destino = value; }
        }

        private List<PasajeroEntity> pasajeros = new List<PasajeroEntity>();

        public List<PasajeroEntity> Pasajeros
        {
            get { return pasajeros; }
            set { pasajeros = value; }
        }

        private AvionEntity avion;

        public AvionEntity Avion
        {
            get { return avion; }
            set { avion = value; }
        }


        private DateTime horaPartida;

        public DateTime HoraPartida
        {
            get { return horaPartida; }
            set { horaPartida = value; }
        }

        private DateTime horaArribo;

        public DateTime HoraArribo
        {
            get { return horaArribo; }
            set { horaArribo = value; }
        }

        private bool activo;

        public bool Activo
        {
            get { return activo; }
            set { activo = value; }
        }



    }
}

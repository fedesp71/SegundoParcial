﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class CiudadEntity
    {
        private string latitud;

        public string Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }
        private string longitud;

        public string Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

    }
}

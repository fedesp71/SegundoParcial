﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace DAL
{
    public class McAvion
    {
        Acceso acceso = new Acceso();
        public AvionEntity ObtenerAvion(int id)
        {
            AvionEntity avion = new AvionEntity();
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@IdAvion", id));

            DataTable tabla = acceso.Leer("spObtenerAvion", parameters);
            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {
                avion.Id = Convert.ToInt32(registro["Id"].ToString());
                avion.Nombre = registro["Nombre"].ToString();
                avion.Capacidad = Convert.ToInt32(registro["Capacidad"].ToString());
            }
            return avion;
        }

        public int Alta(AvionEntity avion)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Nombre", avion.Nombre));
            parameters.Add(acceso.CrearParametro("@Capacidad", avion.Capacidad));

            return acceso.Escribir("spAltaAvion", parameters);
        }

        public int Baja(AvionEntity avion)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", avion.Id));

            return acceso.Escribir("spBajaAvion", parameters);
        }

        public int Modifiacion(AvionEntity avion)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", avion.Id));
            parameters.Add(acceso.CrearParametro("@Nombre", avion.Nombre));
            parameters.Add(acceso.CrearParametro("@Capacidad", avion.Capacidad));

            return acceso.Escribir("spModifAvion", parameters);
        }


        public List<AvionEntity> Listar()
        {
            List<AvionEntity> aviones = new List<AvionEntity>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarAviones");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                AvionEntity avion = new AvionEntity();
                avion.Id = int.Parse(registro["Id"].ToString());
                avion.Nombre = registro["Nombre"].ToString();
                avion.Capacidad = int.Parse(registro["Capacidad"].ToString());

                aviones.Add(avion);
            }
            return aviones;
        }
    }
}

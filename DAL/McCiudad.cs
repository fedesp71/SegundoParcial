﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace DAL
{
    public class McCiudad
    {
        Acceso acceso = new Acceso();
        public CiudadEntity ObtenerCiudad(int id)
        {
            CiudadEntity ciudad = new CiudadEntity();
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@IdCiudad", id));
            DataTable tabla = acceso.Leer("spObtenerCiudad");
            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {
                ciudad.Id = Convert.ToInt32(registro["Id"].ToString());
                ciudad.Nombre = registro["Nombre"].ToString();
                ciudad.Latitud = registro["Latitud"].ToString();
                ciudad.Longitud = registro["Longitud"].ToString();
            }
            return ciudad;
        }


        public int Alta(CiudadEntity ciudad)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Nombre", ciudad.Nombre));
            parameters.Add(acceso.CrearParametro("@Latitud", ciudad.Latitud));
            parameters.Add(acceso.CrearParametro("@Longitud", ciudad.Longitud));

            return acceso.Escribir("spAltaCiudad", parameters);
        }

        public int Baja(CiudadEntity ciudad)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", ciudad.Id));

            return acceso.Escribir("spBajaCiudad", parameters);
        }

        public int Modifiacion(CiudadEntity ciudad)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", ciudad.Id));
            parameters.Add(acceso.CrearParametro("@Nombre", ciudad.Nombre));
            parameters.Add(acceso.CrearParametro("@Latitud", ciudad.Latitud));
            parameters.Add(acceso.CrearParametro("@Longitud", ciudad.Longitud));

            return acceso.Escribir("spModifCiudad", parameters);
        }


        public List<CiudadEntity> Listar()
        {
            List<CiudadEntity> ciudades = new List<CiudadEntity>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarCiudad");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                CiudadEntity ciudad = new CiudadEntity();
                ciudad.Id = int.Parse(registro["Id"].ToString());
                ciudad.Nombre = registro["Nombre"].ToString();
                ciudad.Latitud = registro["Latitud"].ToString();
                ciudad.Longitud = registro["Longitud"].ToString();

                ciudades.Add(ciudad);
            }
            return ciudades;
        }
    }
}

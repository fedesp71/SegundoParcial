﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace DAL
{
    public class McVuelo
    {
        Acceso acceso = new Acceso();
        public int Alta(VueloEntity vuelo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            //parameters.Add(acceso.CrearParametro("@Id", vuelo.Id));
            parameters.Add(acceso.CrearParametro("@IdAvion", vuelo.Avion.Id));
            parameters.Add(acceso.CrearParametro("@IdCiudad", vuelo.Destino.Id));
            parameters.Add(acceso.CrearParametro("@HoraPartida", vuelo.HoraPartida));
            parameters.Add(acceso.CrearParametro("@HoraArribo", vuelo.HoraArribo));
            int i = acceso.Escribir("spAltaVuelo", parameters);
            acceso.Cerrar();
            return i;
        }


        public int AltaPasajero(VueloEntity vuelo, PasajeroEntity pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@IdVuelo", vuelo.Id));
            parameters.Add(acceso.CrearParametro("@IdPasajero", pasajero.Id));

            int i = acceso.Escribir("spAltaPasajerosVuelo", parameters);
            acceso.Cerrar();
            return i;
        }

        public int Cancelacion(VueloEntity vuelo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", vuelo.Id));

            int i = acceso.Escribir("spCancelarVuelo", parameters);
            acceso.Cerrar();
            return i;
        }

        public List<VueloEntity> ListarVuelosxCombinacion(int idCiudad)
        {
            List<VueloEntity> vuelos = new List<VueloEntity>();
            //spListarVuelosxCiudad
            return vuelos;
        }

        public List<VueloEntity> ListarVuelosActivos()
        {
            List<VueloEntity> vuelos = new List<VueloEntity>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarVuelosActivos");
            acceso.Cerrar();

            McAvion mcAvion = new McAvion();
            McCiudad mcCiudad = new McCiudad();
            foreach (DataRow registro in tabla.Rows)
            {
                VueloEntity vuelo = new VueloEntity();
                vuelo.Id = int.Parse(registro["Id"].ToString());
                vuelo.Avion = mcAvion.ObtenerAvion(int.Parse(registro["IdAvion"].ToString()));
                vuelo.Destino = mcCiudad.ObtenerCiudad(int.Parse(registro["IdCiudad"].ToString()));
                vuelos.Add(vuelo);
            }
            return vuelos;

        }

        public List<VueloEntity> Listar()
        {
            List<VueloEntity> vuelos = new List<VueloEntity>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarVuelos");
            acceso.Cerrar();

            McAvion mcAvion = new McAvion();
            McCiudad mcCiudad = new McCiudad();
            foreach (DataRow registro in tabla.Rows)
            {
                VueloEntity vuelo = new VueloEntity();
                vuelo.Id = int.Parse(registro["Id"].ToString());
                vuelo.Avion = mcAvion.ObtenerAvion(int.Parse(registro["IdAvion"].ToString()));
                //vuelo.Destino = mcCiudad.ObtenerCiudad(int.Parse(registro["IdCiudad"].ToString()));
                vuelos.Add(vuelo);
            }
            return vuelos;

        }


        public List<VueloEntity> ListarVuelosId(List<int> ids)
        {
            List<VueloEntity> vuelos = new List<VueloEntity>();
            foreach(int id in ids)
            {
                acceso.Abrir();
                DataTable tabla = acceso.Leer("spListarVuelos");
                acceso.Cerrar();

                McAvion mcAvion = new McAvion();
                McCiudad mcCiudad = new McCiudad();
                foreach (DataRow registro in tabla.Rows)
                {
                    VueloEntity vuelo = new VueloEntity();
                    vuelo.Id = int.Parse(registro["Id"].ToString());
                    vuelo.Avion = mcAvion.ObtenerAvion(int.Parse(registro["IdAvion"].ToString()));
                    //vuelo.Destino = mcCiudad.ObtenerCiudad(int.Parse(registro["IdCiudad"].ToString()));
                    vuelos.Add(vuelo);
                }
                
            }
            return vuelos;

        }



        public List<VueloEntity> Listar40()
        {        
            acceso.Abrir();
            List<int> nros = new List<int>();
            DataTable tabla = acceso.Leer("spVuelos40");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {       
                if((int.Parse(registro["Pasajeros"].ToString()) * 100 / int.Parse(registro["Capacidad"].ToString())) < 40)
                {
                    nros.Add(int.Parse(registro["Id"].ToString()));
                }
            }
            return ListarVuelosId(nros);
        }

    }
}

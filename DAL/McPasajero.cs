﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace DAL
{
    public class McPasajero
    {
        Acceso acceso = new Acceso();
        public int Alta(PasajeroEntity pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Nombre", pasajero.Nombre));
            
            return acceso.Escribir("spAltaPasajero", parameters);
        }

        public int Baja(PasajeroEntity pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", pasajero.Id));

            return acceso.Escribir("spBajaPasajero", parameters);
        }

        public int Modifiacion(PasajeroEntity pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", pasajero.Id));
            parameters.Add(acceso.CrearParametro("@Nombre", pasajero.Nombre));

            return acceso.Escribir("spModifPasajero", parameters);
        }

        public List<PasajeroEntity> Listar()
        {
            List<PasajeroEntity> pasajeros = new List<PasajeroEntity>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarPasajeros");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                PasajeroEntity pasajero = new PasajeroEntity();
                pasajero.Nombre = registro["Nombre"].ToString();
                pasajero.Id = int.Parse(registro["Id"].ToString());

                pasajeros.Add(pasajero);
            }
            return pasajeros;
        }
    }



}
